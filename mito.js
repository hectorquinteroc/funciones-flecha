let subscriptores = 'Prueba funcion flecha suscriptores';

module.exports = {
    subs : subscriptores,
    saludar: () => {
        console.log('Prueba funcion flecha saludar');
    },
    sumar1: (a, b) => {
        return a + b;
    },
    sumar2: (c, d) => c + d,
    mostrar: (e) => e + 10 
} 